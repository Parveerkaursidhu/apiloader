import React,{useEffect,useState} from 'react'
import Layout from '../Layout';
import List from '../components/List';
import firebaseApp from '../utils/firebaseApp';

export default function Home(props) {
 const [userList, setUserList] = useState([])

 const userValue = ()=>{

    const testCollection = firebaseApp.firestore().collection('reactCollection');
    testCollection.get().then(docs=>{ 
      let tmpUser=[]
      docs.forEach(doc=>{
        console.log("user Data",doc.data());
        tmpUser.push({userId:doc.id,  ...doc.data()})
      })
  
      setUserList(tmpUser)
    }) 
   }
    useEffect(() => { 
      userValue()
    }, [  ])

    const onSubmit = async (evt) =>{
        evt.preventDefault() 
        const res =
        await firebaseApp.firestore().collection('test').add({
            name:evt.target.name.value,
            country: evt.target.city.value,
            img: 'https://i.pravatar.cc/300'
          });
        userValue() 
      }
    
 return(
    <div className = "container">
         
         <Layout  {...props} >
         
            <form onSubmit={onSubmit}></form>
         </Layout>
         </div>
);



}