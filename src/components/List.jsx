import React, { useEffect , useState} from "react";
import './List.css';

export default function List() {

  const [myUsers, setmyUsers] = useState([])
  const geUsers = (url) => {
   return  fetch("https://reqres.in/api/users") 
  };
  
  
  useEffect(() => {
    const data = geUsers();
    data.then((response) => response.json())
    .then((users) => {
      setmyUsers(users.data)
      //console.log("users form HOOK ",users.data); // https://reqres.in/api/users/2
    
    })
    .catch(errr=>{
      console.log("Error",errr);
    });
     
  }, []);

  
  return (
   
    <div className={"row d-flex  jsutify-content-center"} >
    
        {myUsers.map((item, index) => {
          const {id,avatar,first_name,flast_name} = item 
          return <div  class="card m-2" style={{width: '18rem'}}>
          <img class="card-img-top" src={avatar} alt="Card image cap"/>
          <div class="card-body">
              <h5 class="card-title">{`${first_name} ${flast_name}.....`}</h5>           
          </div>
        </div>
        })}
    
       
    </div>
  );
}
