import React from "react";

export default function Form({onSubmit}) {

    
    // const submitData =(evt)=>{
    //         evt.preventDefault()
    //             console.log(evt.target.email.value,evt.target.password.value);

    // }
  return (
    <div>
      <form onSubmit={onSubmit} className="form-group">
        <div class="form-group">
          <label >Email address</label>
          <input
            type="email"
            class="form-control" 
            placeholder="Enter email"
            name={"email"}
          />
          {/* <small id="emailHelp" class="form-text text-muted">
            We'll never share your email with anyone else.
          </small> */}
        </div>
        <div class="form-group">
          <label >Password</label>
          <input
            type="password"
            class="form-control"
            id="exampleInputPassword1"
            placeholder="Password"
            name={"password"}
          />
        </div>

        <button  className={"btn btn-info"} type={"submit"}>Save</button>
      </form>
    </div>
  );
}
