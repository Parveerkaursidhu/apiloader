
import React, { Component }  from 'react';
import Navbar from '../Navbar';

class About extends Component{
    render(){
        return(
            <div className="container">
                  <Navbar/>
                <h1>About US</h1>
            </div>
        );
    }
}

export default About;