import React, { Component } from 'react';

import './App.css';
import Home from '../src/components/Home';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import About from '../src/components/About';
import Contact from '../src/components/Contact';


class App extends Component{
  render() {
    // const {myValue} = this.state;

  return (
    <div className="App">
      
      <Router> 
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/aboutUs" component={About} />
              <Route exact path="/contactUs" component={Contact} />
             
            </Switch>
         
        </Router>

      
     
     
    </div>
  );
}
}

export default App;
