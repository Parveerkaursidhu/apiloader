import React from "react";
import List from "./components/List";
import Navbar from "./Navbar";

class Layout extends React.Component{
    render(){

        const { children}  = this.props
              return(
            <div className="container">
                <Navbar/>
                {children}
            </div>

        );
    }


}

export default Layout;